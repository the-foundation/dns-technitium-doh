mkdir /src &>/dev/null
apk add --no-cache bash curl git make  gcc  musl-dev

which doh-server || (
    mkdir /src/doh
    export PREFIX=/usr/
    git clone https://github.com/m13253/dns-over-https.git /src/doh && cd /src/doh &&  make -j4 doh-server/doh-server
    find /src/doh -type f -executable -name doh-server|while read file ;do cp "$file" /usr/bin;done
    which doh || (
        cd /src/doh
        make install
        ## since upstream might fail again :
        which doh-server || cp /usr/local/bin/doh-server /usr/bin/
        which doh-client || cp /usr/local/bin/doh-client /usr/bin/
    )

    ) &

/bin/bash -c "test -f /usr/bin/dnsproxy || ( cd / && git clone https://github.com/AdguardTeam/dnsproxy.git && cd dnsproxy && go build && mv dnsproxy /usr/bin && cd . . && rm -rf dnsproxy ; )" &

wait
apk del make gcc  musl-dev
