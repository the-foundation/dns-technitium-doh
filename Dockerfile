
#FROM alpine:3.9
#LABEL maintainer "publicarray"
#LABEL description "A DNS-over-HTTP server proxy in Rust. https://github.com/jedisct1/rust-doh"
#ENV REVISION 1
#ENV DOH_BUILD_DEPS rust cargo make
#ENV VERSION 0.1.19
#
#RUN echo 'http://dl-cdn.alpinelinux.org/alpine/edge/community' >> /etc/apk/repositories
#RUN apk add --no-cache $DOH_BUILD_DEPS
#RUN cargo install doh-proxy --version $VERSION --root /usr/local/
## RUN set -x && \
##     cd /tmp && \
##     git clone https://github.com/jedisct1/rust-doh && \
##     cd rust-doh && \
##     cargo build --release && \
##     cp target/release/doh-proxy /usr/local/bin/doh-proxy

#------------------------------------------------------------------------------#
#FROM publicarray/doh-proxy
#RUN echo binary taken from publicarray/doh-proxy
#LABEL maintainer "publicarray"

#FROM roxedus/ts-dnsserver
#ADD . /src

#FROM golang:alpine
FROM golang:1.20-alpine
WORKDIR /src
COPY installer-builder-step0.sh /
RUN apk add  --no-cache bash git make  && cd /src && bash /installer-builder-step0.sh
RUN which doh-server
RUN which dnsproxy
#RUN cp doh-server/doh-server.conf /

#------------------------------------------------------------------------------#
#FROM alpine
#FROM golang:alpine
FROM golang:alpine
COPY --from=0 /usr/bin/dnsproxy /usr/bin/dnsproxy
COPY --from=0 /src/doh/doh-server/doh-server /doh-server
#COPY --from=0 /doh-server.conf /doh-server.conf
COPY doh-server.conf installer.sh /
RUN apk add bash && bash /installer.sh

RUN ls /doh-server /doh-server.conf && chmod +x /doh-server && ls -1 /etc/dns/TechnitiumLibrary.dll &&  ls -1 /bin/dotnet
CMD /bin/bash
ENTRYPOINT /init.sh
